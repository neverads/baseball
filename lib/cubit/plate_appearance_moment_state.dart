part of "plate_appearance_moment_cubit.dart";

@immutable
class PlateAppearanceMomentState extends Equatable {
  const PlateAppearanceMomentState({required this.balls, required this.strikes});

  // final String pitchThrown = "pitch: fastball";
  // final String batterAction = "batter-action: none";
  // final String outcome = "outcome: strike";
  final int balls;
  final int strikes;

  @override
  List<Object?> get props {
    return [balls, strikes];
  }

// @override
// List<Object?> get props {
//   return [balls, strikes]; //, pitchThrown, batterAction, outcome];
// }
}

abstract class PlateAppearanceMomentStateFresh extends PlateAppearanceMomentState {
  final bool wasBaseOnBalls;
  final bool wasStrikeOut;

  const PlateAppearanceMomentStateFresh({
    this.wasStrikeOut = false,
    this.wasBaseOnBalls = false,
  }) : super(
          balls: 0,
          strikes: 0,
        );

  @override
  List<Object?> get props {
    return [balls, strikes, wasBaseOnBalls, wasStrikeOut];
  }
}

class PlateAppearanceMomentStateInitial extends PlateAppearanceMomentStateFresh {
  const PlateAppearanceMomentStateInitial() : super();
}

class PlateAppearanceMomentStateBaseOnBalls extends PlateAppearanceMomentStateFresh {
  const PlateAppearanceMomentStateBaseOnBalls() : super(wasBaseOnBalls: true);
}

class PlateAppearanceMomentStateStrikeOut extends PlateAppearanceMomentStateFresh {
  const PlateAppearanceMomentStateStrikeOut() : super(wasStrikeOut: true);
}
