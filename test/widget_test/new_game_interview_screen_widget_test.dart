import 'package:baseball/UI/interview_summary_screen.dart';
import 'package:baseball/UI/new_game_interview_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("initial launch", () {
    testWidgets("displays tabs, relative tab-view, and continue-button", (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(home: NewGameInterviewScreen()),
      );
      expect(find.text("Interview"), findsOneWidget);

      expect(find.byType(Tab), findsNWidgets(5));
      expect(find.text("Name"), findsOneWidget);
      expect(find.text("Team"), findsOneWidget);
      expect(find.text("Background"), findsOneWidget);
      expect(find.text("Motivation"), findsOneWidget);
      expect(find.text("Traits"), findsOneWidget);

      expect(find.byType(ElevatedButton), findsOneWidget);
      expect(find.text("End interview"), findsOneWidget);

      final nameQuestion = "Hi there - I'm Harriet Greene from the Post.  Let's get this interview started!";
      final teamQuestion = "Rumor has it that you just got signed.  So who was it - which team are you going to lead?";
      final backgroundQuestion = "So tell me about yourself - what's your baseball background?";
      final motivationQuestion = "You're going to be in charge of literally everything.";
      final traitsQuestion = "Everyone has strengths & weaknesses, and these often balance each other.";
      expect(find.text(nameQuestion), findsOneWidget);
      expect(find.text(teamQuestion), findsNothing);
      expect(find.text(backgroundQuestion), findsNothing);
      expect(find.text(motivationQuestion), findsNothing);
      expect(find.text(traitsQuestion), findsNothing);

      await tester.tap(find.text("Team"));
      await tester.pumpAndSettle();
      expect(find.text(nameQuestion), findsNothing);
      expect(find.text(teamQuestion), findsOneWidget);
      expect(find.text(backgroundQuestion), findsNothing);
      expect(find.text(motivationQuestion), findsNothing);
      expect(find.text(traitsQuestion), findsNothing);

      await tester.tap(find.text("Background"));
      await tester.pumpAndSettle();
      expect(find.text(nameQuestion), findsNothing);
      expect(find.text(teamQuestion), findsNothing);
      expect(find.text(backgroundQuestion), findsOneWidget);
      expect(find.text(motivationQuestion), findsNothing);
      expect(find.text(traitsQuestion), findsNothing);

      await tester.tap(find.text("Motivation"));
      await tester.pumpAndSettle();
      expect(find.text(nameQuestion), findsNothing);
      expect(find.text(teamQuestion), findsNothing);
      expect(find.text(backgroundQuestion), findsNothing);
      expect(find.text(motivationQuestion), findsOneWidget);
      expect(find.text(traitsQuestion), findsNothing);

      await tester.tap(find.text("Traits"));
      await tester.pumpAndSettle();
      expect(find.text(nameQuestion), findsNothing);
      expect(find.text(teamQuestion), findsNothing);
      expect(find.text(backgroundQuestion), findsNothing);
      expect(find.text(motivationQuestion), findsNothing);
      expect(find.text(traitsQuestion), findsOneWidget);
    });
  });

  group("end interview button", () {
    testWidgets("navigates to interview-summary-screen", (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(home: NewGameInterviewScreen()),
      );
      await tester.tap(find.text("End interview"));
      await tester.pumpAndSettle();
      expect(find.byType(InterviewSummaryScreen), findsOneWidget);
    });
  });
}
