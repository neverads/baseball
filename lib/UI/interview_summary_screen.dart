import 'package:baseball/UI/manage_team_screen.dart';
import 'package:baseball/data/team_model.dart';
import 'package:flutter/material.dart';

import 'constants.dart';

class InterviewSummaryScreen extends StatefulWidget {
  final String summary;

  const InterviewSummaryScreen(this.summary);

  @override
  _InterviewSummaryScreenState createState() => _InterviewSummaryScreenState();
}

class _InterviewSummaryScreenState extends State<InterviewSummaryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Interview summary"),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                const ListTile(
                  title: const Text(
                    "Well, I think that's all I need for my article.  "
                    "If you want to go back and change any answers, feel free!",
                  ),
                ),
                const ListTile(
                  title: const Text("Here are my summary-notes:"),
                ),
                ListTile(
                  title: Text(widget.summary),
                ),
              ],
            ),
          ),
          continueButton,
        ],
      ),
    );
  }

  Widget get continueButton {
    return Padding(
      padding: paddingButtonContinueBottom,
      child: ElevatedButton(
        child: Text("Continue"),
        onPressed: navigateToManageTeam,
      ),
    );
  }

  Future<void> navigateToManageTeam() async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => const ManageTeamScreen(
          Team(city: "Tampa Bay"),
        ),
      ),
    );
  }
}
