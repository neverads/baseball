import 'package:baseball/UI/new_game_interview_screen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  final String title;

  const HomeScreen({required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Card(
              child: ListTile(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => NewGameInterviewScreen(),
                  ),
                ),
                leading: Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                title: Text("New game"),
                subtitle: Text("Start a new management career."),
              ),
            ),
            loadGameWidget,
            settingsWidget,
          ],
        ),
      ),
    );
  }

  // Widget get newGameWidget {
  //   return Card(
  //     child: ListTile(
  //       onTap: () => Navigator.push(context, route).push(route),
  //       leading: Icon(
  //         Icons.star,
  //         color: Colors.amber,
  //       ),
  //       title: Text("New game"),
  //       subtitle: Text("Start a new franchise."),
  //     ),
  //   );
  // }

  Widget get loadGameWidget {
    return Card(
      child: ListTile(
        enabled: false,
        leading: Icon(
          Icons.slow_motion_video,
          color: Colors.amber,
        ),
        title: Text("Resume"),
        subtitle: Text("Continue building your legacy."),
      ),
    );
  }

  Widget get settingsWidget {
    return Card(
      child: ListTile(
        enabled: false,
        leading: Icon(
          Icons.settings,
          color: Colors.amber,
        ),
        title: Text("Settings"),
        subtitle: Text("Customize to your heart's content."),
      ),
    );
  }
}
