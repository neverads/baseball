import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'game_in_progress_state.dart';


class GameInProgressCubit extends Cubit<GameInProgressState> {
  GameInProgressCubit() : super(GameInProgressStateInitial());
}