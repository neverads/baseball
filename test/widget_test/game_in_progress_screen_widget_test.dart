import 'package:baseball/UI/game_in_progress_screen.dart';
import 'package:baseball/cubit/game_in_progress_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockGameInProgressCubit extends MockCubit<GameInProgressState> implements GameInProgressCubit {}

class GameInProgressStateFake extends Fake implements GameInProgressState {}

void main() {
  setUpAll(() => registerFallbackValue(GameInProgressStateFake()));
  late MockGameInProgressCubit cubit;

  setUp(() {
    cubit = MockGameInProgressCubit();
    when(() => cubit.state).thenReturn(GameInProgressStateInitial());
  });

  group("initial launch", () {
    testWidgets("loads basic UI", (WidgetTester tester) async {
      await tester.pumpWidget(
        RepositoryProvider.value(
          value: cubit,
          child: MaterialApp(
            home: GameInProgressScreen(),
          ),
        ),
      );
      expect(find.byType(LineScoreWidget), findsOneWidget);
    });
  });
}
