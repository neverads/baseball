import 'package:baseball/UI/home_screen.dart';
import 'package:baseball/data/plate_appearance/plate_appearance_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(BaseballSimApp(
    plateAppearanceModel: PlateAppearanceModel(),
  ));
}

class BaseballSimApp extends StatelessWidget {
  final PlateAppearanceModel plateAppearanceModel;

  const BaseballSimApp({required this.plateAppearanceModel});

  @override
  Widget build(BuildContext context) {
    final String appTitle = "Baseball Simulator";
    final MaterialColor primarySwatch = Colors.green;
    final Color primaryColor = primarySwatch.shade700;
    final Color accentColor = Colors.amber;

    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider.value(value: plateAppearanceModel),
      ],
      child: MaterialApp(
        title: appTitle,
        theme: ThemeData(
          primarySwatch: primarySwatch,
          primaryColor: primaryColor,
          accentColor: accentColor,
          tabBarTheme: TabBarTheme(
            labelColor: Colors.black87,
            unselectedLabelColor: Colors.black54,
          ),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(primary: primaryColor),
          ),
        ),
        darkTheme: ThemeData(
          brightness: Brightness.dark,
          primarySwatch: primarySwatch,
          primaryColor: primaryColor,
          accentColor: accentColor,
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(primary: primaryColor),
          ),
        ),
        home: HomeScreen(title: appTitle),
      ),
    );
  }
}
