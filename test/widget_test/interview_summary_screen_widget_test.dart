import 'package:baseball/UI/interview_summary_screen.dart';
import 'package:baseball/UI/manage_team_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final summary = "my summary";

  group("initial launch", () {
    testWidgets("displays explanation/intro & summary", (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: InterviewSummaryScreen(summary),
        ),
      );
      expect(find.text("Interview summary"), findsOneWidget);
      expect(find.text("Here are my summary-notes:"), findsOneWidget);
      expect(find.text(summary), findsOneWidget);
      expect(find.byType(ElevatedButton), findsOneWidget);
      expect(find.text("Continue"), findsOneWidget);
    });
  });

  group("continue button", () {
    testWidgets("navigates to manage-team-screen", (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(home: InterviewSummaryScreen(summary)),
      );
      await tester.tap(find.text("Continue"));
      await tester.pumpAndSettle();
      expect(find.byType(ManageTeamScreen), findsOneWidget);
    });
  });
}
