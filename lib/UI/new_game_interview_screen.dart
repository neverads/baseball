import 'package:baseball/UI/interview_summary_screen.dart';
import 'package:baseball/UI/manage_team_screen.dart';
import 'package:baseball/data/team_model.dart';
import 'package:flutter/material.dart';

import 'constants.dart';

class NewGameInterviewScreen extends StatefulWidget {
  const NewGameInterviewScreen();

  @override
  _NewGameInterviewScreenState createState() => _NewGameInterviewScreenState();
}

class _NewGameInterviewScreenState extends State<NewGameInterviewScreen> {
  static const List<Tab> _tabs = [
    const Tab(text: "Name"),
    const Tab(text: "Team"),
    const Tab(text: "Background"),
    const Tab(text: "Motivation"),
    const Tab(text: "Traits"),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Interview"),
        ),
        body: Column(
          children: [
            TabBar(tabs: _tabs),
            Expanded(
              child: TabBarView(
                children: [
                  nameWidget,
                  teamWidget,
                  backgroundWidget,
                  motivationWidget,
                  traitsWidget,
                ],
              ),
            ),
            endInterviewButton,
          ],
        ),
      ),
    );
  }

  Widget get nameWidget {
    return ListView(
      children: [
        const ListTile(
          title: const Text("Hi there - I'm Harriet Greene from the Post.  Let's get this interview started!"),
        ),
        const ListTile(
          title: const Text("First, how do you spell your name?  (I wouldn't want to get the basics wrong.)"),
        ),
        const SizedBox(height: 16),
        const Text("<< input for player's handle/name >>"),
      ],
    );
  }

  Widget get teamWidget {
    return ListView(
      children: [
        const ListTile(
          title:
              const Text("Rumor has it that you just got signed.  So who was it - which team are you going to lead?"),
        ),
        const SizedBox(height: 16),
        const Text("<< button to go to team-selection >>"),
      ],
    );
  }

  Widget get backgroundWidget {
    return ListView(
      children: [
        const ListTile(
          title: const Text("So tell me about yourself - what's your baseball background?"),
        ),
        const SizedBox(height: 16),
        const Text("<< input for player's history as stat-nerd, former all-star/bench-rider, etc (job vs hobby) >>"),
      ],
    );
  }

  Widget get motivationWidget {
    return ListView(
      children: [
        const ListTile(
          title: const Text("You're going to be in charge of literally everything."),
        ),
        const ListTile(
          title: const Text("From signing & trading players to calling for a slider, you pull all the strings."),
        ),
        const ListTile(
          title: const Text("Why do you do it?  What drives you?"),
        ),
        const SizedBox(height: 16),
        const Text("<< input for personality/behavior/style indicators >>"),
      ],
    );
  }

  Widget get traitsWidget {
    return ListView(
      children: [
        const ListTile(
          title: const Text("Everyone has strengths & weaknesses, and these often balance each other."),
        ),
        const ListTile(
          title: const Text("What are some such traits that you recognize in yourself?"),
        ),
        const SizedBox(height: 16),
        const Text("<< input for choosing traits, which have positives & negatives to each >>"),
      ],
    );
  }

  Widget get endInterviewButton {
    return Padding(
      padding: paddingButtonContinueBottom,
      child: ElevatedButton(
        onPressed: navigateToInterviewSummary,
        child: const Text("End interview"),
      ),
    );
  }

  Future<void> navigateToInterviewSummary() async {
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => const InterviewSummaryScreen("<< placeholder summary >>"),
      ),
    );
  }
}

// todo -- present reorder-able lists so the user can choose what is and is not relatively important
// todo  + or allow each option to be rated on a scale, having a set amount of motivation-points to allocate
