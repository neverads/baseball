part of 'game_in_progress_cubit.dart';

@immutable
abstract class GameInProgressState extends Equatable {
  const GameInProgressState();

  @override
  bool? get stringify => true;

  @override
  List<Object?> get props => throw UnimplementedError();
}

class GameInProgressStateInitial extends GameInProgressState {
  final String homeInitials = "TB";
  final String awayInitials = "OT";

  final int homeScore = 0;
  final int awayScore = 0;

  final String currentInning = "↑1";

  const GameInProgressStateInitial();
}
