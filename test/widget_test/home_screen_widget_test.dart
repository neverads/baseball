import 'package:baseball/UI/home_screen.dart';
import 'package:baseball/UI/new_game_interview_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final title = "PC load letter";

  group("initial launch", () {
    testWidgets("displays initial UI", (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: HomeScreen(title: title),
        ),
      );
      expect(find.text(title), findsOneWidget);
      expect(find.byType(ListTile), findsNWidgets(3));
      expect(find.byType(Icon).evaluate().map((evaluated) => (evaluated.widget as Icon).icon), [
        Icons.star,
        Icons.slow_motion_video,
        Icons.settings,
      ]);
      expect(find.text("New game"), findsOneWidget);
      expect(find.text("Start a new management career."), findsOneWidget);
      expect(find.text("Resume"), findsOneWidget);
      expect(find.text("Continue building your legacy."), findsOneWidget);
      expect(find.text("Settings"), findsOneWidget);
      expect(find.text("Customize to your heart's content."), findsOneWidget);
    });
  });

  group("tap new game button", () {
    testWidgets("navigates to the NewGameScreen", (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: HomeScreen(title: title),
        ),
      );
      await tester.tap(find.text("New game"));
      await tester.pumpAndSettle();
      expect(find.byType(NewGameInterviewScreen), findsOneWidget);
    });
  });
}
