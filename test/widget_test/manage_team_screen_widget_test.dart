import 'package:baseball/UI/game_in_progress_screen.dart';
import 'package:baseball/UI/manage_team_screen.dart';
import 'package:baseball/data/team_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("initial launch", () {
    testWidgets("loads basic UI", (WidgetTester tester) async {
      const teamName = "PC load letter";

      await tester.pumpWidget(
        MaterialApp(
            home: ManageTeamScreen(
          Team(city: teamName),
        )),
      );
      expect(find.text("My team ($teamName)"), findsOneWidget);

      expect(find.byType(Tab), findsOneWidget);
      expect(find.text("Next game"), findsOneWidget);
      expect(find.byType(GamePreviewWidget), findsOneWidget);
      expect(find.byType(StartGameButton), findsOneWidget);
    });
  });

  group("start game button", () {
    testWidgets("navigates to in-progress game screen", (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(home: ManageTeamScreen(Team(city: ""))),
      );
      await tester.tap(find.text("Start game"));
      await tester.pumpAndSettle();
      expect(find.byType(GameInProgressScreen), findsOneWidget);
    });
  });
}
