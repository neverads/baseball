part of 'plate_appearance_model.dart';

class Count {
  int _balls = 0;
  int _strikes = 0;

  void reset() {
    _balls = 0;
    _strikes = 0;
  }

  int balls() {
    return _balls;
  }

  int strikes() {
    return _strikes;
  }

  void incrementBalls() {
    _balls++;
  }

  void incrementStrikes() {
    _strikes++;
  }
}
