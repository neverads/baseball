part 'plate_appearance_outcomes.dart';
part 'count.dart';


class PlateAppearanceModel {
  final Count _count = Count();

  Count get count {
    return _count;
  }

  void reset() {
    _count.reset();
  }

  PlateAppearanceOutcomes? outcome() {
    if (count.balls() == 4) {
      return PlateAppearanceOutcomes.baseOnBalls;
    } else if (count.strikes() == 3) {
      return PlateAppearanceOutcomes.strikeOut;
    } else {
      return null;
    }
  }
}
