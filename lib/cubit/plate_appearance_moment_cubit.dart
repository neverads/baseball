import 'package:baseball/data/plate_appearance/plate_appearance_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import "package:flutter_bloc/flutter_bloc.dart";

part "plate_appearance_moment_state.dart";

class PlateAppearanceMomentCubit extends Cubit<PlateAppearanceMomentState> {
  final PlateAppearanceModel _data;

  PlateAppearanceMomentCubit(this._data) : super(PlateAppearanceMomentStateInitial());

  void ballPitched() {
    _data.count.incrementBalls();
    if (_data.outcome() == PlateAppearanceOutcomes.baseOnBalls) {
      _data.reset();
      emit(
        PlateAppearanceMomentStateBaseOnBalls(),
      );
    } else {
      emit(
        PlateAppearanceMomentState(
          balls: _data.count.balls(),
          strikes: _data.count.strikes(),
        ),
      );
    }
  }

  void strikePitched() {
    _data.count.incrementStrikes();
    if (_data.outcome() == PlateAppearanceOutcomes.strikeOut) {
      _data.reset();
      emit(
        PlateAppearanceMomentStateStrikeOut(),
      );
    } else {
      emit(
        PlateAppearanceMomentState(
          balls: _data.count.balls(),
          strikes: _data.count.strikes(),
        ),
      );
    }
  }
}
