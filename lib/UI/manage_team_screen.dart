import 'package:baseball/UI/constants.dart';
import 'package:baseball/UI/game_in_progress_screen.dart';
import 'package:baseball/data/team_model.dart';
import 'package:flutter/material.dart';

class ManageTeamScreen extends StatelessWidget {
  final Team team;

  const ManageTeamScreen(this.team);

  @override
  Widget build(BuildContext context) {
    final List<Tab> _tabs = [
      Tab(text: "Next game"),
    ];

    return DefaultTabController(
      length: _tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text("My team (${team.city})"),
        ),
        body: Column(
          children: [
            TabBar(tabs: _tabs),
            Expanded(
              child: TabBarView(
                children: [
                  nextGameTabView,
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get nextGameTabView {
    return Column(
      children: [
        GamePreviewWidget(
          homeTeam: team,
          awayTeam: Team(
            city: "Other Team",
          ),
        ),
        StartGameButton(),
      ],
    );
  }
}

class GamePreviewWidget extends StatelessWidget {
  final Team homeTeam;
  final Team awayTeam;

  const GamePreviewWidget({
    required this.homeTeam,
    required this.awayTeam,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  const Text("Home"),
                  TeamPreviewWidget(homeTeam),
                ],
              ),
              Column(
                children: [
                  const Text("Away"),
                  TeamPreviewWidget(awayTeam),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class TeamPreviewWidget extends StatelessWidget {
  final Team team;

  const TeamPreviewWidget(this.team);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(team.city),
      ],
    );
  }
}

class StartGameButton extends StatelessWidget {
  const StartGameButton();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: paddingButtonContinueBottom,
      child: ElevatedButton(
        child: Text("Start game"),
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => GameInProgressScreen(),
          ),
        ),
      ),
    );
  }
}
