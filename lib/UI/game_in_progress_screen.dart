import 'package:baseball/cubit/game_in_progress_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GameInProgressScreen extends StatelessWidget {
  const GameInProgressScreen();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => GameInProgressCubit(),
      // RepositoryProvider.of<ClipboardInterface>(context),
      child: GameInProgressView(),
    );
  }
}

class GameInProgressView extends StatefulWidget {
  const GameInProgressView();

  @override
  _GameInProgressViewState createState() => _GameInProgressViewState();
}

class _GameInProgressViewState extends State<GameInProgressView> {
  GameInProgressCubit get cubit => context.read<GameInProgressCubit>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: BlocBuilder<GameInProgressCubit, GameInProgressState>(
          builder: (context, state) {
            if (state is GameInProgressStateInitial) {
              return LineScoreWidget(state);
            } else {
              return Container();
            }
          },
        ),
      ),
      body: Column(
        children: [
          Container(),
        ],
      ),
    );
  }
}

class LineScoreWidget extends StatelessWidget {
  final GameInProgressStateInitial gameState;

  const LineScoreWidget(this.gameState);

  @override
  Widget build(BuildContext context) {
    final String homeSummary = "${gameState.homeInitials} ${gameState.homeScore}";
    final String awaySummary = "${gameState.awayInitials} ${gameState.awayScore}";
    return Text("$homeSummary    $awaySummary    ${gameState.currentInning}");
  }
}
